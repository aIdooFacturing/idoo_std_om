	var canvas;
	var ctx;
 
	var darkmask;
	var rect;

	var lightArray = new Array();
	var lightingArray = new Array();
	
	$(function() {
//		canvas = document.getElementById("canvas");
//		ctx = canvas.getContext("2d");
//		
//		canvas.width = window.innerWidth;
//		canvas.height = window.innerHeight;
		
		//drawObject();
		
		setInterval(time, 1000);
		//setTimeout(movingObjAnim, 1000);
	});
	
	
	function movingObjAnim(){
		$("#logo").css("z-index",999);
		$("#logo").animate({
			right: window.innerWidth/2 - $("#logo").width()/2-400,
			top : window.innerHeight/2 - $("#logo").height()/2-500,
			width : 1200
		},4000,"swing",function(){
			$("#logo").animate({
				top : 170,
				right : 60,
				width : 412,
				height : 379
			},4000,"swing", function(){
				$("#corver").css("z-index",-9);
				
				setTimeout(function(){
					$("#logo").css("z-index",1);
					$("#corver").css("z-index",10);
					$("#video").css("z-index",999);
					
					$("#video").animate({
						left: window.innerWidth/2 - $("#video").width()/2-400,
						top : window.innerHeight/2 - $("#video").height()/2-500,
						width : 1200
					}, 4000, "swing", function(){
						setTimeout(function(){
							$("#video").animate({
								left: 10,
								top : 101,
								width : 520
							},4000,"swing", function(){
								$("#video").css("z-index",1);
								$("#corver").css("z-index",-9);
								//alarm();
								
								setTimeout(function(){
									$("#corver").css("z-index",99);
									
									//entire running
									$('#myTurnCanvas').width("1200");
									$("#myTurnHolder").width("1200");
									$('#myTurnCanvas').height("1200");
									$("#myTurnHolder").height("1200");
									
									$("#rGaugeHolder").hide('slow');
									$('#myTurnCanvas').parent().css('z-index', 1);
									$("#myTurnHolder").show('slow');
									
									$("#myTurnHolder").css('z-index', 999);
									
									setTimeout(function(){
										$("#corver").css("z-index",-9);
										
										$("#rGaugeHolder").show('slow');
										$("#myTurnHolder").hide('slow');
										setTimeout(alarm,2000);
									/*	setTimeout(function(){
											//each running
											var ctx = document.getElementById("myTurnCanvas").getContext("2d");
											window.myLine = new Chart(ctx).Radar(radarChartData, {
												responsive: true
												,scaleFontSize: 40
												,tooltipFontSize: 40
												,tooltipTitleFontSize: 40
											});
											
											$('#myTurnCanvas').width("1200");
											$("#myTurnHolder").width("1200");
											$('#myTurnCanvas').height("1200");
											$("#myTurnHolder").height("1200");
											
											$("#lGaugeHolder").hide('slow');
											$('#myTurnCanvas').parent().css('z-index', 1);
											$("#myTurnHolder").show('slow');
											
											$("#myTurnHolder").css('z-index', 999);
											
										}, 2000);*/
									},2000)
									
								},2000)
							});
						},2000);
					});
				},2000);
			});
		});
	};
	
	function barAnim(){
		if(direction=="right"){
			dist+=4;
		}else if(direction=="left"){
			dist-=4;
		};
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function time(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date").html(month + " / " + date_ + " (" + day + ")");
		$("#time").html(hour + " : " + minute + " : " + second);
	};
	
	//3840 2059
	//1615 938
	var width = window.innerWidth;
	var height = window.innerHeight;
	
	var objArray = [
		            	//	[550, 318, 470, 470/1.8, "MCM25", "green","MCM25"],
		            	//	[950, 285, 550, 550/1.9, "DCM37100F", "green","DCM37100F"],
		            	//	[550, 510, 480, 480/1.42, "HF7P", "green", "HF7P"],
		            	//	[1056, 510, 470, 470/1.68, "5FPM", "green", "5FPM"],

		            	//	[780, 870, 550, 550/2.28, "BG20", "green", "BG20"],
		            		
		            	//	[550, 985, 580, 580/2.1, "BG40", "green", "BG40"],
		            	//	[1100, 1070, 400, 400/1.95, "BG30", "green", "BG30"],
		            	//	[1360, 1300, 140, 140/1.8, "AH8J", "green", "AH8J"],
		            	//	[1360, 1400, 130, 130/1.8, "AH9J", "green", "AH9J"],
		            		
		            //		[2535, 120, 350, 350/1.37, "HM1250W", "green", "HM1250W"],
		            		
		            //		[1585, 328, 290, 290/1.35, "NB13W", "green", "NB13W"],
		            //		[1935, 328, 290, 290/1.35, "NB14W", "green", "NB14W"],
		           // 		[2485, 330, 230, 230/0.77, "5FMS", "green", "5FMS#3"],
		           // 		[2655, 330, 230, 230/0.77, "5FMS", "green", "5FMS#2"],
		          //  		[2825, 330, 230, 230/0.77, "5FMS", "green", "5FMS#1"],
		          //  		[1960, 515, 175, 175/0.9, "HFMS", "green", "HFMS#1"],
		         //   		[2040, 515, 175, 175/0.9, "HFMS", "green", "HFMS#2"],
		        //    		[2120, 515, 175, 175/0.9, "HFMS", "green", "HFMS#3"],
		         //   		[2200, 515, 175, 175/0.9, "HFMS", "green", "HFMS#4"],
		          //  		
		        //    		[2050, 720, 350, 350/1.57, "AH20-6P", "green", "AH20-6P"],
		        //    		[1607, 882, 440, 440/1.51, "HF3M", "green", "HF3M"],
		            		
		          //  		[2400, 750, 370, 370/2, "PROTH", "green", "PROTH"],
		          //  		[2355, 865, 390, 390/1.79, "BG10", "green", "BG10"],
		          //  		[2665, 865, 460, 460/1.91, "DCM2780F", "green", "DCM2780F"],
		            		
		          //  		[1970, 980, 620, 620/2.29, "HF7M", "green", "HF7M"],
		            		
		        //    		[1630, 1400, 100, 100/0.53, "YBM1530V", "green", "YBM1530V"],
		         //   		[1690, 1320, 220, 220/0.66, "HM2J", "green", "HM2J"],
		         //   		[1880, 1390, 130, 130/0.61, "HM1J", "green", "HM1J"],
		        //    		[1990, 1250, 150, 150/1.39, "AH1J", "green", "AH1J"],
		         //   		[2040, 1430, 110, 110/0.71, "AH2J", "green", "AH2J"],
		            		
		       //     		[2485, 1180, 220, 220/0.8, "GRI10", "green", "GRI10"],
		      //      		[2600, 1203, 250, 250/1.82, "PF62", "green", "PF62"],
		       //    		[2780, 1203, 250, 250/1.82, "PF61", "green", "PF61"],
		            		
		        //    		[2720, 1410, 200, 200/0.89, "MODI", "green", "MODI"],
		            		
	                	];		
	
	function chagneColor(name, src){
		for(var i = 0; i < objArray.length; i++){
			if(objArray[i][6]==name){
				objArray[i][4]=src;
			};
		};
	};
	
	var first = true;
	function drawObject(){
		lightArray = new Array();
		lightingArray = new Array();
		
		//도형 그리기
		for(var i = 0; i < objArray.length; i++){
			var src = new Image();
			src.src  = "../images/DashBoard/" + objArray[i][4] + ".svg";
			
			//src,x,y,w,h
			ctx.drawImage(src, objArray[i][0], objArray[i][1], objArray[i][2], objArray[i][3]);
			
			//모델명
			ctx.fillStyle = "white";
			ctx.font = "15px Arial";
			
			//text,x,y
			ctx.fillText(objArray[i][6], (objArray[i][0] + objArray[i][2]) - (objArray[i][2]/2)-25, (objArray[i][1] + objArray[i][3]) - (objArray[i][3]/2)+10);
			
			var light = new Array();
			
			///x,y,w,h
			light.push(objArray[i][0]+40, (objArray[i][1] + objArray[i][3]) - (objArray[i][3]/2)+20, 10, 5);
			lightArray.push(light);
		};
		
		if(first){
			//anim();
		};
		first = false;
		
		/*dist+=2;
		for(var i = 0 ; i < lightArray.length; i++){
			//방향
			add = dist+2;
			ctx.beginPath();
			ctx.rect(lightArray[i][0]+add,lightArray[i][1],lightArray[i][2],lightArray[i][3]);
			ctx.stroke();
			ctx.fillStyle = "white";
			ctx.fill();
		};*/
	};
	var add;
	var dist = 2;
	var direction = "right";
	function anim() {
		var dist = 0 ;
			
		setInterval(function(){
			ctx.clearRect(0,0, canvas.width, canvas.height)
			//drawObject();
		});
	};
	
	function alarm() {
		bodyNeonEffect("red");
		
		chagneColor("HF3M", "HF3M_RED");
		
		$("#alarm_line").css("display","inline");
		
		setTimeout(function(){
			$("#alarm_line").attr("src","../images/DashBoard/Alarm_HF3M.svg");
			setTimeout(function(){
				$("#alarm_line").attr("src","");
				bodyNeonEffect("blue");
				chagneColor("HF3M", "HF3M");
			}, 2000);
		},2000);
	};
	
	function bodyNeonEffect(color) {
		var lineWidth = 70;
		var toggle = true;
		setInterval(function() {
			$("#svg").css("box-shadow",
					"inset 0px 0px " + lineWidth + "px " + color);

			if (toggle) {
				lineWidth -= 2;
				if (lineWidth == 0) {
					toggle = false;
				}
			} else if (!toggle) {
				lineWidth += 2;
				if (lineWidth == 70) {
					toggle = true;
				}
			};
		},50);
	};
	