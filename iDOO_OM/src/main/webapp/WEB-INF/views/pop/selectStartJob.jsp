<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="/WEB-INF/views/include/unomiclib.jsp"%>
<%@ include file="/WEB-INF/views/pop/lib.jsp"%>
<%@ page import="com.unomic.dulink.common.domain.CommonCode"%>
<%@ page import="com.unomic.dulink.chart.domain.*"%>
<%@ page session = "true" %>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<style>

table input[type=number]{
	width : 170;
	font-size: 200%;
	letter-spacing: 9px;
	text-align: center;
}
table select{
	font-size: 120%;
}
table input[type=button] {
	margin-top:7%;
	margin-left:5%;
	font-size: 300%;
	border-radius: 7%;
	padding-left: 3%;
	padding-right: 3%;
    background: aliceblue;
    cursor: pointer;
}
</style>

<script>

	var evtMsg;
	var nm = '<%=(String)session.getAttribute("nm")%>';
		nm = decode(nm);
	var empCd = '<%=(String)session.getAttribute("empCd")%>';
	
	$(function(){
//		$("#barcode").val("9ck_5")
		//session 있는지 체크
		sessionChk();

		getTable();
		
		$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		msg();
		
		//focus TEXT 맞추기
		var chk_short = true;
		
		$(document).bind("keydown keyup", function(e) {
	        var key = e.keyCode;
	        var tg = e.target;
	        if(tg.tagName == "INPUT" ||  tg.tagName == "TEXTAREA") return true;
	        
	        var specific = key >= 8 && key <= 46;
	        if(e.type == "keydown") {
	            if(specific) {
	                chk_short = false;
	                return true;
	            }
	            if(!specific && chk_short) {
	            	 $("#barcode").focus().select();
	                //target_input.focus().select(); return false;
	            }
	            if(e.ctrlKey && e.keyCode == 86){
	            	 $("#barcode").focus().select();
	            }
	        } else {
	            if(specific) {
	                chk_short = true;
	            }
	        }
	    });
		
		//시간
		setInterval(function() {
			$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		}, 1000);
	})
	
	
	function sessionChk(){
		if(empCd==null || empCd=="null"){
			location.href='${ctxPath}/pop/popIndex.do';
		}
		
	}
		//enter key event
	function enterEvt(event) {
		if(event.keyCode == 13){
			getTable()
		}
	}
	//사원 바코드를 입력해주세요
	function alarmMsg(){
		clearTimeout(evtMsg)
		return evtMsg = setTimeout(function() {$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText'>["+ nm +"] 님 소재 바코드를 입력해주세요.</marquee></span>")}, 10000)
	}
	function msg(){
		$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText'>["+ nm +"] 님 소재 바코드를 입력해주세요.</marquee></span>")
	}
	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	function getParameterByName(paramName){ 
		var _tempUrl = window.location.search.substring(1); 
		//url에서 처음부터 '?'까지 삭제 
		var _tempArray = _tempUrl.split('&'); 
		
		// '&'을 기준으로 분리하기 
		for(var i = 0; _tempArray.length; i++) { 
			var _keyValuePair = _tempArray[i].split('='); 
				// '=' 을 기준으로 분리하기 
			if(_keyValuePair[0] == paramName){ 
				// _keyValuePair[0] : 파라미터 명 
				// _keyValuePair[1] : 파라미터 값
				return _keyValuePair[1]; 
			 }
		 }
	}
	//session chk
	function sessionChk(){
		if(empCd==null || empCd=="null"){
			location.href='${ctxPath}/pop/popIndex.do';
		}
		
	}
	
	function saveRow(){
		sessionChk();
		if(empCd==null || empCd=="null"){
			return;
		}
		
		var flag = 0;
		var url = "${ctxPath}/pop/StartJobSave.do";
		
		if(json[0].stock < Number($("#stock").val())){
			$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText' style='color:red;'>이동수량이 재고수량보다 많을수 없습니다.</marquee></span>")
			alarmMsg2()
			return;
		}else if(Number($("#stock").val())==0){
			$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText' style='color:red;'>이동할 수량이 없습니다.</marquee></span>")
			alarmMsg2()
			return;
		}
		
		var sPrdNo;
		var sCnt
		var sProj;
		var sBeforeProj;
		
		sPrdNo = json[0].prdNo;
		sCnt = json[0].cnt;
		sProj = ty;
		/* if(flag==0){
			alert("관리자 문의해주세요.")
			return;
		} */
		
		var param = "empCd=" + empCd+
					"&name=" + name +
					"&dvcId=" + dvcId +
					"&ty=" + ty +
					"&prdNo=" + sPrdNo +
					"&barcode=" + json[0].barcode +
					"&cnt=" + sCnt +
					"&proj=" + sProj +
					"&beforeProj=" + sBeforeProj +
//					"&idx=" + json[0].idx +
					"&stock=" + Number($("#stock").val());

		console.log(param);
		$.showLoading(); 
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
//			dataType : "json",
			success : function(data){
				console.log(data)
				if(data=="success"){
					$.hideLoading(); 
					alert("작업시작 정상적으로 시작되었습니다.");
//					location.href="${ctxPath}/pop/popIndex.do;"
					
				}else if(data=="fail"){
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:blue;'>관리자에게 문의해주세요.</marquee></span>")
					alarmMsg();
				}
			}
		})
		
	}
			
	function getTable(){
		if($("#barcode").val()==""){
			return;
		}
		
		var name = getParameterByName('name');
		
		$("#backBtn span").html(decode(name))
		dvcId = getParameterByName('dvcId');
		ty = getParameterByName('ty');

		
		$("#barcode").focus();
		$("#barcode").select();
		
		var barcode=$("#barcode").val();
		
		$.showLoading(); 
		
		var url = "${ctxPath}/pop/getHistoryList.do";
		
		
		
		var param = "barcode=" + barcode +
					"&name=" + name +
					"&NM=" + nm +
					"&dvcId=" + dvcId + 
					"&empCd=" + empCd +
					"&ty=" + ty;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				name = decode(name)
				nm = decode(nm)
				ty = ty;
				console.log(json)
//				var table = "<table id='test' style='width:100%;text-align:center; font-size:200%; border-spacing: 0 20;'><tr><td colspan='6' style='text-align:right;'><input type='button' id='addDevice' value='장비추가' style='vertical-align: middle; margin-right:5%; font-size:200%; '></td></tr>"
				var	table = "<table id='test' style='width:100%;text-align:center; font-size:200%; border-spacing: 0 20;'><tr><th>장비</th><th>소재</th><th>공정</th><th>총수량</th><th>재고수량</th><th>이동수량</th></tr>"
//					table += '<tr><td colspan="6"><input type="button" value="asdas" style="vertical-align: middle; margin-left: 50%;"></td></tr>'
				var select;
					select = "<select id='selctPrdNo'>"
				$(json).each(function(idx,data){
					
					select += "<option>" + data.beforePrdNo +"</option>"
					
				});
				
				var tyNm="";
				if(ty=="0010"){
					tyNm = "R삭"
				}else if(ty=="0020"){
					tyNm = "MCT"
				}else if(ty=="0030"){
					tyNm = "CNC"
				}else{
					tyNm = ty;
				}
				
				select += "</select>";
				
//				table += "<tr><td>" + nm + "</td>"
				table += "<td>" + name + "</td>"
				table += "<td>" + json[0].prdNo + "</td>"
				table += "<td>" + tyNm + "</td>"
//				table += "<td>" + json[0].barcode + "</td>"
				table += "<td id='cnt'>" + json[0].cnt + "</td>"
				table += "<td id='cnt'>" + json[0].stock + "</td>"
				table += "<td> <input type='number' id='stock' value='" + json[0].stock + "' readonly></td></tr>"

//				table += "<td><label class='plus' onclick='cntPlus(100)'>▲ </label> <label class='plus' onclick='cntPlus(10)'> ▲ </label> <label class='plus' onclick='cntPlus(1)'> ▲ </label><br><input type='number' id='stock' value='" + json[0].cnt + "'><br><label class='minus' onclick='cntMinus(-100)'>▼ </label> <label class='minus' onclick='cntMinus(-10)'> ▼ </label> <label class='minus' onclick='cntMinus(-1)'> ▼ </label><br></td></tr>"
				/* $(json).each(function(idx,data){
					data.name = decode(data.name);
					data.nm = decode(data.nm);

					table += "<tr><td>" + nm + "</td>"
					table += "<td>" + name + "</td>"
					table += "<td>" + data.prdNo + "</td>"
					table += "<td>" + data.cnt + "</td>"
					table += "<td> <input type='number' id='stock' value='" + data.cnt + "'></td></tr>"
						
				}); */
					

				table += "<tr><td colspan='6'><input type='button' value='확인' onclick='saveRow()'>　<input type='button' value='취소' onclick='history.back(-1);'></td></tr>"

				$("#jobList").empty()
				$("#jobList").append(table)
				
				clearTimeout(evtMsg)
				$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText' style='color:blue;'>["+ nm +"] 님 작업시작 하시겠습니까?</marquee></span>")
				
				// select 박스 변경시 event
//				changeEvt();

				$.hideLoading(); 
			},error : function(request,status,error){
				if(request.responseText=="no"){
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:red;'>불출된 소재바코드가 없습니다.</marquee></span>")
					$("#jobList").empty()
					alarmMsg();
				}else if(request.responseText=="working"){
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:red;'>현재 작업이 진행중인 소재입니다</marquee></span>")
					$("#jobList").empty()
					alarmMsg();
				}
				else{
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:blue;'>관리자에게 문의해주세요.</marquee></span>")
					$("#jobList").empty()
					alarmMsg();
					
				}
				$.hideLoading(); 
//				 alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
				   


			}
		});
	}
	
	
</script>

<body>
	<div id="header">
		<div id="leftH">
			<span></span>
		</div>
		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
			<span>부광정밀공업(주)</span>
		</div>
		<div id="rightH">
			<span id="time"></span>
		</div>
	</div>
	<div id="aside">
		<span>
			<marquee behavior=alternate scrollamount="20">
			</marquee>
		</span>
	</div>
	<div id="content">
		<div id="searchText" align="center" style="font-size: 200%; height: 10%">
			바코드 : <input type="text" id="barcode" onkeyup="enterEvt(event)" style="vertical-align: middle;">
		</div>
		<div id="jobList">
		</div>
	</div>
</body>
</html>