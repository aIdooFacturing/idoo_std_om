<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
html{
	overflow : hidden;
}
#grid .k-grid-header th.k-header{
	background-color: black;
	color:white;
}
#datePicker{
    background : rgb(121, 218, 76);
    border-radius: 5px;
    font-weight: bolder;
}
</style> 
<script type="text/javascript">
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	var handle = 0;
	
	function getDvcList(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		
		var url =  ctxPath + "/chart/getDvcNameList.do";
		var param = "shopId=" + shopId + 
					"&line=ALL" + 
					"&sDate=" + today + 
					"&eDate=" + today + " 23:59:59" + 
					"&fromDashboard=" + fromDashboard;
		
		dvcArray = [];
		
		$.ajax({
			url : url,
			data :param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var list = "<option value='all'>${total}</option>";
				
				$(json).each(function(idx, data){
					dvcArray.push(data.id)
					list += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";					
				});

				$("#group").html(list);
				
				getAlarmData();
			}
		});	
	};
	var grid;
	$(function(){
		setDate();
		getDvcList();
		//getGroup();
		createNav("kpi_nav", 8);
		
		setEl();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		
		grid = $("#grid").kendoGrid({
			height:getElSize(1650),
			columns: [{
                field: "name",
                title: "${device}",
                width: getElSize(300),
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },
            {
                title: "${alarm}<br>${start_time}",
                field: "startDateTime",
                width: getElSize(600),
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },
            {
                title: "${alarm}<br>${end_time}",
                field: "endDateTime",
                width: getElSize(600),
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },
            {
                title: "${fixed_time}",
                field: "delayTimeSec",
                width: getElSize(300),
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },
            {
                title: "${alarm}${content}",
                field: "alarm",
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            }]
			}).data("kendoGrid");
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
		});
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#contentDiv").css({
			"overflow" : "auto",
			"width" : $(".menu_right").width()
		});
		
		$("#insertForm").css({
			"width" : getElSize(3000),
			"position" : "absolute",
			"z-index" : 999,
		});
		
		$("#insertForm").css({
			"left" : $(".menu_right").offset().left + ($(".menu_right").width()/2) - ($("#insertForm").width()/2) - marginWidth,
			"top" : getElSize(400)
		});
		
		$("#insertForm table td").css({
			"font-size" : getElSize(70),
			"padding" : getElSize(15),
			"background-color" : "#323232"
		});
		
		$("#insertForm button, #insertForm select, #insertForm input").css({
			"font-size" : getElSize(60),
			"margin" : getElSize(15)
		});
		
		$(".table_title").css({
			"background-color" : "#222222",
			"color" : "white"
		});
		
		$("#contentTable td").css({
			"color" : "white",
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#sDate, #eDate").css({
			"width" : getElSize(300)
		})
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function getGroup(){
		var url = "${ctxPath}/chart/getGroup.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "<option value='all'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.group + "'>" + data.group + "</option>"; 
				});
				
				$("#group").html(option);
				
				getAlarmData();
			}
		});
	};
	
	var className = "";
	var classFlag = true;
	var dataSource;
	
	
	function getAlarmData(){
		dataSource = new kendo.data.DataSource({});
		
		
		$("#print_table").empty();
		
		var table ="<table border='1' class='top_table' width='95%' align='center'><tr>"+
		"<th rowspan='3' colspan='2'>알람 발생 이력 <br>"+ 
		"<br>"+ $("#sDate").val() +'~' + $("#eDate").val() +"</th><th colspan='3' style='background-color:lightgray'>결제</th></tr>" +
		"<tr class='sign_title' style='background-color:lightgray'><th>담당</th><th>검토</th><th>승인</th></tr>" +
		"<tr class='sign' ><th> </th><th> </th><th> </th></tr>" +
		"</table>"+
		"<table class='p_table' border='1' width='95%' align='center'>"+
		"<tr style='background-color:lightgray'><th>장비</th><th>알람 발생 시간</th><th>알람 해제 시간</th><th>조치 시간</th><th>알람 내용</th>" ;
		
		var url = "${ctxPath}/chart/getAlarmData.do";
		var sDate = $("#sDate").val();
		var eDate = $("#eDate").val();
		
		var param = "sDate=" + sDate +
					"&eDate=" + eDate + 
					"&shopId=" + shopId + 
					"&dvcId=" + $("#group").val();
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.alarmList;
				
				var tr = "<thead><Tr style='font-weight: bolder;background-color: rgb(34,34,34)' class='thead'>" + 
							"<td>${device}</td>" + 
							"<td>${alarm}<br>${start_time}</td>" + 
							"<td>${alarm}<br>${end_time}</td>" + 
							"<td>${fixed_time}</td>" + 
							"<td style='width: 50%'>${alarm}${content}</td>" + 
						"</tr></thead><tbody>";
						
				csvOutput = "${device},${alarm}${start_time}, ${alarm}${end_time}, ${fixed_time}, ${alarm}${content}LINE";
				$(json).each(function(idx, data){
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;
					
					var endDateTime;
					
					if(data.endDateTime!=null){
						endDateTime = data.endDateTime.substr(0,19);
					};
					var delayTimeSec = data.delayTimeSec;
					
					if((data.endDateTime == null || data.endDateTime=="") && (data.delayTimeSec == null || data.delayTimeSec=="")){
						endDateTime = "-";
						delayTimeSec = "처리 중";
					}else{
						delayTimeSec = Math.ceil(delayTimeSec/60)
					}
					
					var alarm;
					if(data.ncAlarmNum1!=""){
						data.alarm =  data.ncAlarmNum1 + "-" + decodeURIComponent(data.ncAlarmMsg1).replace(/\+/gi, " ");
					};
					if(data.ncAlarmNum2!=""){
						data.alarm = data.ncAlarmNum2 + "-" + decodeURIComponent(data.ncAlarmMsg2).replace(/\+/gi, " "); 
					};
					if(data.ncAlarmNum3!=""){
						data.alarm = data.ncAlarmNum3 + "-" + decodeURIComponent(data.ncAlarmMsg3).replace(/\+/gi, " "); 
					};
					
					data.name = decode(data.name)
					data.startDateTime = data.startDateTime.substr(0,19)
					data.endDateTime = data.endDateTime
					data.delayTimeSec = delayTimeSec
					
					tr += "<tr class='contentTr " + className + "'>" +
								"<td>" + decode(data.name) + "</td>" +
								"<td>" + data.startDateTime.substr(0,19) + "</td>" + 
								"<td>" + endDateTime + "</td>" + 
								"<td>" + delayTimeSec + "</td>" + 
								"<td>" + decodeURIComponent(alarm) + "</td>";
						"</tr>";
					
					csvOutput += data.name + "," + 
								data.startDateTime.substr(0,19) + "," + 
								endDateTime + "," + 
								Math.ceil(delayTimeSec/60) + "," + 
								decodeURIComponent(alarm) + "LINE";
					
					dataSource.add(data)
					
				});
				
				dataSource.fetch(function(){
					$.hideLoading(); 
					console.log(dataSource)
				});
				
				grid.setDataSource(dataSource);
				
				
				var dataItem=grid.dataSource.data();
			
			
			//달성률로 object 정렬
			dataItem.sort(function (a, b) {
				return b.dvcId - a.dvcId;
			});
			
			console.log(dataItem)
			//print table 그리는 부분  (행:9)

			var chk=1;
			
			//프린트 그리는 총 테이블 갯수 구하기 chk
			$(dataItem).each(function(idx,data){
				chk++;
				if(idx!=0){
					if(dataItem[idx].name!=dataItem[idx-1].name){
						chk++;
					}
				}
			});
			//테이블 몇번째 열 까지 뽑을지 (A4사이즈 맞추기 위해서)
			var divideNum=28;		
			var page=1;		//page size표시
			var totalpage=Math.ceil(chk/divideNum);		//총 페이지수
			var index=0;	//행 갯수 세기
			var sameName=0;
			$(dataItem).each(function(idx,data){
				
					//한개의 tr
					if(idx==0){
						index++;
						table +="<tr style='background-color:lightgray' class='totalwork'><td colspan='5'> 장비명 : "+ data.name +"</td>";
					}else{	
						index++;
						if(dataItem[idx].name!=dataItem[idx-1].name){
							index++;
							table +="<tr style='background-color:lightgray' class='totalwork'><td colspan='5'> 장비명 : "+ data.name +"</td>";
						}
					}
					
					//tr
					table +="<tr><td>" + data.name + "</td><td>" +	data.startDateTime + "</td><td>" + data.endDateTime + "</td><td>" + data.delayTimeSec + "</td><td>" + data.alarm;
					
					//a4 size page 넘기기
					if(idx!=0 && index>divideNum ){	
						page++;
						index=1;
						table +="</table>" +
								"<div style='page-break-before:always'></div>";
								
						table +="<br><br><br><br> <table class='p_table' border='1' width='95%' align='center'><tr class='sign'><th colspan='4' rowspan='2'>알람 발생 이력"+
								"<br>"+ $("#sDate").val() +'~' + $("#eDate").val() +"</th>	<th style='background-color:lightgray' colspan='2'>페이지</th></tr>" +
								"<tr class='sign'><th colspan='2'>" + page +" / " + totalpage +"</th></tr>" +
								"<tr style='background-color:lightgray'><th>장비</th><th>알람 발생 시간</th><th>알람 해제 시간</th><th>조치 시간</th><th>알람 내용</th>" ;
								
					}
			})
			
			table +="</table>"
			$("#print_table").append(table);
			
			//print css
			$(".sign").css({
				"height" : "50px"
			})
			$(".sign th").css({
				"width" : "70px"
			})

			$(".p_table tr th").css("font-size","10px")
			$(".p_table tr td").css("font-size","10px")
				
				
				
				
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(5) + "px solid rgb(50,50,50)"
				});
				
				$("#wrapper").css({
					"height" :getElSize(1650),
					"overflow" : "hidden"
				});
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$("#wrapper div:last").remove();
				scrolify($('.alarmTable'), getElSize(1400));
				$("#wrapper div:last").css("overflow", "auto")
				
				$("#grid").css({
					"background-color":"gray",
					"color" : "white"
				}) 
				
				$("#grid tr:odd").css({
				      "background-color":"rgb(50,50,50)",
				      "color" : "white"
			   });
				   
				$("#grid tr:even").css({
				    "background-color":"rgb(33,33,33)",
				    "color" : "white"
				});
				
				$(".top_table").css({
					"font-size" : "10px"
				})
				
			}
		});
	};
	
	function pop_print(){
		win = window.open();
        self.focus();
        win.document.open();
        win.document.write('<'+'html'+'><'+'head'+'><'+'style'+'>');
        win.document.write('body, td { font-family: Verdana; font-size: 10pt;}');
        win.document.write('<'+'/'+'style'+'><'+'/'+'head'+'><'+'body'+'>');
        win.document.write(document.getElementById('print_table').innerHTML);
        win.document.write('<'+'/'+'body'+'><'+'/'+'html'+'>');
        win.document.close();
        win.print();
        win.close();
	}
	
	
	$(function(){
		$("input#sDate").datepicker({
			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
			onSelect:function(){
			}
		})
		$("input#eDate").datepicker({
			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
			onSelect:function(){
			}
		})
		$(".day").css({
			"background" : "green",
			"color" : "white"
		})
	})

	var startDate,
	endDate,
	selectCurrentWeek = function () {
	    window.setTimeout(function () {
	        $('#sDate,#eDate').datepicker('widget').find('.ui-datepicker-current-day a').addClass('ui-state-active')
	    }, 1);
	};

	function daily(){
		
		$( "input#sDate" ).datepicker( "destroy" );
		$( "input#eDate" ).datepicker( "destroy" );
		
		$('.ui-weekpicker').on('mousemove', 'tr', function () {
	       $(this).find('td a').removeClass('ui-state-hover');
	   });
		
		$("input#sDate").datepicker({
		    "minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
			onSelect:function(){
			}
		})
		$("input#eDate").datepicker({
		    "minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
			onSelect:function(){
			}
		}) 
		
		$("#dailyRange").css({
			"display" : "inline"
		})
		$("#weeklyRange").css({
			"display" : "none"
		})
		$("#monthlyRange").css({
			"display" : "none"
		})
		
		$(".day").css({
			"background" : "green",
			"color" : "white"
		})
		$(".week").css({
			"background" : "rgb(121, 218, 76)",
			"color" : "black"
		}) 
		$(".month").css({
			"background" : "rgb(121, 218, 76)",
			"color" : "black"
		})
		$("#actionBtn").css({
				"display":"inline"
		})
	}

	function weekly(){
		
		$( "input#sDate" ).datepicker( "destroy" );
		$( "input#eDate" ).datepicker( "destroy" );
		
		$('input#sDate').datepicker({
		 "minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
	       "showOtherMonths": false,
	       "selectOtherMonths": false,
	       "onSelect": function (dateText, inst) {
	           var date = $(this).datepicker('getDate'),
	               dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
	           startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
	           endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
	           $('#sDate').val($.datepicker.formatDate(dateFormat, startDate, inst.settings));
	           $('#eDate').val($.datepicker.formatDate(dateFormat, endDate, inst.settings));
	           selectCurrentWeek();
	       },
	       "beforeShow": function () {
	           selectCurrentWeek();
	       },
	       "beforeShowDay": function (date) {
	           var cssClass = '';
	           if (date >= startDate && date <= endDate) {
	               cssClass = 'ui-datepicker-current-day';
	           }
	           return [true, cssClass];
	       },
	       "onChangeMonthYear": function (year, month, inst) {
	           selectCurrentWeek();
	       }
	   }).datepicker('widget').addClass('ui-weekpicker');
	   
	   $('input#eDate').datepicker({
		   "minDate": new Date(2017, 12-1, 1),
	       "maxDate": 0,
	       "showOtherMonths": false,
	       "selectOtherMonths": false,
	       "onSelect": function (dateText, inst) {
	           var date = $(this).datepicker('getDate'),
	               dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
	           startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
	           endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
	           $('#sDate').val($.datepicker.formatDate(dateFormat, startDate, inst.settings));
	           $('#eDate').val($.datepicker.formatDate(dateFormat, endDate, inst.settings));
	           selectCurrentWeek();
	       },
	       "beforeShow": function () {
	           selectCurrentWeek();
	       },
	       "beforeShowDay": function (date) {
	           var cssClass = '';
	           if (date >= startDate && date <= endDate) {
	               cssClass = 'ui-datepicker-current-day';
	           }
	           return [true, cssClass];
	       },
	       "onChangeMonthYear": function (year, month, inst) {
	           selectCurrentWeek();
	       }
	   }).datepicker('widget').addClass('ui-weekpicker');
	   
	   
	   $('.ui-weekpicker').on('mousemove', 'tr', function () {
	       $(this).find('td a').addClass('ui-state-hover');
	   });
	   $('.ui-weekpicker').on('mouseleave', 'tr', function () {
	   	$(this).find('td a').removeClass('ui-state-hover');
	   });
		
		$("#dailyRange").css({
			"display" : "inline"
		})
		$("#monthlyRange").css({
			"display" : "none"
		})
		$(".day").css({
			"background" : "rgb(121, 218, 76)",
			"color" : "black"
		})
		$(".week").css({
			"background" : "green",
			"color" : "white"
		})
		$(".month").css({
			"background" : "rgb(121, 218, 76)",
			"color" : "black"
		})
		$("#actionBtn").css({
			"display":"inline"
		})
	}

	function monthly(){
		$( "input#sDate" ).datepicker( "destroy" );
		$( "input#eDate" ).datepicker( "destroy" );
		
		$('input#sDate').datepicker( {
			 "minDate": new Date(2017, 12-1, 1),
		        "maxDate": 0,
	       changeMonth: true,
	       changeYear: true,
	       showButtonPanel: true,
	       dateFormat: 'yy mm',
	       onClose: function(dateText, inst) { 
	           var month = Math.abs($("#ui-datepicker-div .ui-datepicker-month :selected").val()) + 1;
	           var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	           if(month<10){
	           	month = '0'+month.toString()
	           }
	           
	           $('input#sDate').val(year+'-'+month.toString()+'-'+'01');
	           $('input#eDate').val(year+'-'+month.toString()+'-'+31);
	       }
	   });
		
		$('input#eDate').datepicker( {
			 "minDate": new Date(2017, 12-1, 1),
		        "maxDate": 0,
	       changeMonth: true,
	       changeYear: true,
	       showButtonPanel: true,
	       dateFormat: 'yy mm',
	       onClose: function(dateText, inst) { 
	           var month = Math.abs($("#ui-datepicker-div .ui-datepicker-month :selected").val()) + 1;
	           var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	           if(month<10){
	           	month = '0'+month.toString()
	           }
	           
	           $('input#sDate').val(year+'-'+month.toString()+'-'+'01');
	           $('input#eDate').val(year+'-'+month.toString()+'-'+31);
	       }
	   });
		
		$("#dailyRange").css({
			"display" : "inline"
		})
		
		$(".day").css({
			"background" : "rgb(121, 218, 76)",
			"color" : "black"
		})
		$(".week").css({
			"background" : "rgb(121, 218, 76)",
			"color" : "black"
		})
		$(".month").css({
			"background" : "green",
			"color" : "white"
		})
		$("#actionBtn").css({
			"display":"inline"
		})
	}


</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/mainten_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<table id="contentTable" style="width:100%">
						<Tr>
							<td>
								<spring:message code="device"></spring:message>
								<select id="group"></select>
								<div class="rangeSelector" style="display: -webkit-inline-box;">
									<button id="datePicker" class="day" onclick="daily()">일간</button>					
									<button id="datePicker" class="week" onclick="weekly()">주간</button>					
									<button id="datePicker" class="month" onclick="monthly()">월간</button>
								</div>
								<div id="dailyRange" style="display: inline">
								<spring:message code="op_period"></spring:message>
								<input type="text" class="date" id="sDate" readonly> ~ <input type="text" class="date" id="eDate" readonly>
								</div>
								<div id="actionBtn" style="display: inline; float: right; margin:auto;">
								<button id="search" onclick="getAlarmData()" style="cursor: pointer;"><i class="fa fa-search" aria-hidden="true"></i></button>
								<button id="print" onclick="pop_print()"><i class="fa fa-print" aria-hidden="true"></i> 프린트</button>
								</div>
							</td>
						</Tr>
					</table>
					<div id="grid" style="background-color:black;"></div>
					<!-- <div id="wrapper">
						<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" border="1">
						</table>
					</div> -->
				</td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class="nav_span"></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
		</table>
	 </div>
	<div id="print_table" style="display: none;"></div>
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	