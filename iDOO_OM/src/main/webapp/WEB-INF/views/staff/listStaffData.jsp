<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:choose>
	<c:when test="${!empty listStaff}">
   		<c:forEach var="staff" items="${listStaff}" varStatus="status">
			<tr onclick="fncGetStaffInfo('${staff.staffId}');return false;" class="listData">
				<!-- 
			    <td>${pp.countItem - (pp.maxResults * (pp.currentPage - 1) + (status.count - 1))} </td>
			     -->
			    <td>${staff.rownum }</td>
			    <td>${staff.shopName }</td>			
			    <td>${staff.staffName }</td>
			    <td>${staff.position }</td>
			    <td>${staff.phone }</td>
			</tr>			
		</c:forEach>
		
		<script>
			makePagingButton("${pv.currentPage}","${pv.startPage}","${pv.endPage}","${pv.totalPage}","fncListStaffData");
		</script>
	</c:when>		
	<c:otherwise>
		<tr>					
			<td colspan="9" align="center">no Data.</td>	
		</tr>
	</c:otherwise>
</c:choose>
