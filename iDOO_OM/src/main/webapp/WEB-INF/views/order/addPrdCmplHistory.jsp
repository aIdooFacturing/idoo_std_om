<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}
#grid .k-grid-header th.k-header{
	background-color: black;
	color:white;
}
.k-widget.k-multiselect{
    vertical-align: middle;
    display: inline-block;
}
.k-grid-header th.k-header>.k-link{
	color:white;
}
.k-grid td{
	color:white !important;
}
.k-grid .k-grouping-row td, .k-grid .k-hierarchy-cell{
	background:dimgray;
}
td.k-group-cell{
	background:dimgray;
}
.k-alt{
	background:rgb(50, 50, 50);
}
a.k-grid-filter.k-state-active{
	background-color: rgba( 255, 255, 255, 0 );
	color:yellow;
}
.reset{
	border: 1px solid yellow;
    background: limegreen;
    border-radius: 5px;
    font-weight: bolder;
}
.reset:hover{
	background: green;
	color:white;
}
#datePicker{
    background : rgb(121, 218, 76);
    border-radius: 5px;
    font-weight: bolder;
}
.rangeSelector{
	margin-left: 5px;
}

</style> 
<script type="text/javascript">
	var grid;
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$("#sDate").val(year + "-" + month + "-" + day);
		$("#eDate").val(year + "-" + month + "-" + day);
	};
	
	var handle = 0;
	
	$(function(){
		getPrdNo();
		setDate();

		createNav("kpi_nav", 1);
		
		setEl();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			/* "margin-left" : getElSize(20),
			"margin-right" : getElSize(20) */
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#wrapper").css({
			"height" :getElSize(1550),
			"width" : $(".right").width(),
			"overflow" : "auto"
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#grid").css({
			"width" : getElSize(3350)
		})
		
		$("#print").css({
			"cursor" : "pointer",
			"width" : getElSize(250)
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
			"font-size" : getElSize(50)
		})
		
		$("#multiselect").css({
			"width" : getElSize(500)
		})
		
		$("#workIdx").css({
			"margin-right" : getElSize(100)
		})
		$("#sDate, #eDate").css({
			"width" : getElSize(300)
		})
		$(".rangeSelector").css({
			"margin-left" : getElSize(100)
		})
		
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	

	function getPrdNo(){
		$.showLoading();
		var url = "${ctxPath}/common/getPrdNo.do";
		var param = "shopId=" + shopId;
		prdNo_dataSource = new kendo.data.DataSource();
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				//var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					prdNo_dataSource.add(data)
					//option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
				});
				$.hideLoading()
				//$("#group").html(option);
				multiselect.setDataSource(prdNo_dataSource)
				getPrdData();
			}
		});
	};
	
	var popup = false;
	var dataSource;
	
	function getPrdData(){
		
		$.showLoading();
		
		var sDate = moment($("#sDate").val());
		var eDate = moment($("#eDate").val());
		var diff = eDate.diff(sDate, 'days');
		var dayList = [];
		for(var i=1;i<=diff;i++){
			sDate = moment($("#sDate").val());
			var Date=sDate.add(i,'days').format("YYYY-MM-DD");
			dayList.push(Date);
		}		
		
		
		var sDate = moment($("#sDate").val()).format("YYYY-MM-DD");
		var Date = moment($("#sDate").val()).add(1,'days').format("YYYY-MM-DD");
		
		dataSource = new kendo.data.DataSource({});
		
		$("#print_table").empty();
		
		var table ="<table class='p_table' border='5' width='95%' align='center'><tr><th colspan='5' rowspan='3'>생산완료입력 조회" +
								"<br>"+ $("#sDate").val() +'~' + $("#eDate").val() +"</th><th style='background-color:lightgray' colspan='3'>결제</th></tr>" +
								"<tr style='background-color:lightgray'><th>담당</th><th>검토</th><th>승인</th></tr>" +
								"<tr class='sign' ><th> </th><th> </th><th> </th></tr>" +
								"<tr style='background-color:lightgray'><th>차종</th><th>공정</th><th>장비</th><th>목표수량</th><th>실적수량</th><th>달성율</th><th>불량</th><th>양품율</th></tr>" ;
								
								
		var url = "${ctxPath}/order/getWorkerCnt.do";
		var param = "sDate=" + $("#sDate").val() +
					"&eDate=" + $("#eDate").val() + 
					"&prdNoList=" + multiselect.value() + 
					"&empCdList=" + 
					"&workIdx=" + $("#workIdx").val() + 
					"&dayList=" + dayList + 
					"&waitTime=1";
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				$(json).each(function(idx, data){
						var nd;
						if(data.nd==1){
							data.nd = "${night}"
						}else{
							data.nd = "${day}"
						};
						var oprNo;
						if(data.oprNm=="0010"){
							data.oprNo = "R삭";
						}else if(data.oprNm=="0020"){
							data.oprNo = "MCT 1차";
						}else if(data.oprNm=="0030"){
							data.oprNo = "CNC 2차";
						}
						
						data.name = decode(data.name)
						data.worker = decode(data.worker)
						var diff = moment(data.eDate).diff(moment(data.sDate), 'hours')
						if(isNaN(diff)){
						}else{
							data.workTime=diff
						}
						data.goalRatio = Number(data.goalRatio);
						data.diff_cnt = (Number(data.workerCnt) - Number(data.cnt))
						dataSource.add(data)
					
				});
				dataSource.group([{field:'average', dir:'desc', aggregates: [ 
					{ field:'worker', aggregate: 'max' },
					{ field:'average', aggregate : 'max'},
					{ field:'name', aggregate : 'count'},
					]},{field:'worker', aggregates: [
						{ field:'sDate', aggregate : 'max'},
						{ field:'worker', aggregate: 'max' },
						{ field:'average', aggregate : 'max'}]
					}]);
				
				/* dataSource.fetch(function(){
				}); */
				
				grid.setDataSource(dataSource);
				
				
				$("div.k-textbox.k-space-right").find("input").attr("placeholder","날짜입력")
				
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(5) + "px solid black"
				});
				
				$("#grid").css({
					"background-color":"gray",
					"color" : "white"
				}) 
				
				$("#grid tr:odd").css({
				      "background-color":"rgb(50,50,50) !important",
				      "color" : "white"
			   });
				   
				$("#grid tr:even").css({
				    "background-color":"rgb(33,33,33) !important",
				    "color" : "white"
				});
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$("#wrapper div:last").remove();
				
				$("#wrapper div:last").css("overflow", "auto")
				
				$("button").css("font-size",getElSize(40));
				
				$("body, html").css({
					"overflow" : "hidden"	
				})
				
			var dataItem=grid.dataSource.data();
			
					
			//달성률로 object 정렬
			dataItem.sort(function (a, b) {
				if(a.average == b.average){
					var x = a.worker, y=b.worker;
					return x<y ? -1 : x>y ? 1 : 0;
				}
				return b.average - a.average;
			});
			//print table 그리는 부분  (행:9)
	
			var chk=1;
			
			//프린트 그리는 총 테이블 갯수 구하기 chk
			$(dataItem).each(function(idx,data){
				chk++;
				if(idx!=0){
					if(dataItem[idx].worker!=dataItem[idx-1].worker){
						chk++;
					}
				}
			});
			//테이블 몇번째 열 까지 뽑을지 (A4사이즈 맞추기 위해서)
			var divideNum=37;		
			var page=1;		//page size표시
			var totalpage=Math.ceil(chk/divideNum);		//총 페이지수
			var index=0;	//행 갯수 세기
			var rank=1;
			
			$(dataItem).each(function(idx,data){
				
					//한개의 tr
					if(idx==0){
						index++;
						if(rank>3){
							table +="<tr style='background-color:lightgray' class='totalwork'><td colspan='8'> 작업자 : "+ data.worker +"<font style='float:right'>총 달성율 : " + data.average +" %</font></td>";
						}else{
							table +="<tr style='background-color:lightgray' class='totalwork'><td colspan='8'> 작업자 : "+ data.worker +"<font style='padding-left:50px'>순위 : "+ rank+"위</font><font style='float:right'>총 달성율 : " + data.average +" %</font></td>";
						}
						rank++;
					}else{	
						index++;
						if(dataItem[idx].worker!=dataItem[idx-1].worker){
							index++;
							if(rank>3){
								table +="<tr style='background-color:lightgray' class='totalwork'><td colspan='8'> 작업자 : "+ data.worker +"<font style='float:right'>총 달성율 : " + data.average +" %</font></td>";
							}else{
								table +="<tr style='background-color:lightgray' class='totalwork'><td colspan='8'> 작업자 : "+ data.worker +"<font style='padding-left:50px'>순위 : "+ rank+"위</font><font style='float:right'>총 달성율 : " + data.average +" %</font></td>";
							}
							rank++;
						}
					}
					
					//tr
					table +="<tr><td>" + data.prdNo + "</td><td>" +	data.oprNo + "</td><td>" + data.name + "</td><td>" + data.target + "</td><td>" + data.cnt + "</td><td>" + data.goalRatio + "</td><td>" + data.faultCnt + "</td><td>" + data.okRatio  + "</td></tr>" ;
					
					//a4 size page 넘기기
					if(idx!=0 && index>divideNum ){	
						page++;
						index=1;
						table +="</table>" +
								"<div style='page-break-before:always'></div>";
								
						table +="<br><br><br><br> <table class='p_table' border='5' width='95%' align='center'><tr class='sign'><th colspan='7' rowspan='2'>생산완료입력 조회"+
								"<br>"+ $("#sDate").val() +'~' + $("#eDate").val() +
								"</th><th colspan='2' style='background-color:lightgray'>페이지</th></tr>" +
								"<tr class='sign'><th colspan='2'>" + page +" / " + totalpage +"</th></tr>" +
								"<tr style='background-color:lightgray'><th>차종</th> <th>공정</th> <th>장비</th> <th>목표수량</th> <th>실적수량</th> <th class='sign'>달성율</th> <th class='sign'>불량</th> <th class='sign'>양품율</th></tr>";
								
					}
			})
			
			table +="</table>"
			$("#print_table").append(table);
			
			//print css
			$(".sign").css({
				"height" : "50px"
			})
			$(".sign th").css({
				"width" : "70px"
			})

			$(".p_table tr th").css("font-size","11px")
			$(".p_table tr td").css("font-size","11px")
			
			$(".k-widget.k-multiselect").css({
    			"width": getElSize(500)+"px"
    		})
			
			$.hideLoading(); 
		},error:function(request,status,error){
			$.hideLoading(); 
	        alert("불러오기 오류 발생 !!     code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	    }
	});
};
	
	
	$(function() {
	    
    	grid = $("#grid").kendoGrid({
    		scrollable: true,
    		height:getElSize(1500),
    		sortable : true,
    		filterable: {
    			messages: {
    		          search: "${search_Placeholder}"
    		        }
            },
    		columns: [
   		    {
   		        field: "average",
   		        hidden: true,
   		     	groupHeaderTemplate: '달성율 : #=(aggregates.average.max>1)?aggregates.average.max + "%":"없음"# , '
   		     	+ '총 #=aggregates.name.count# 대  '
   		     },
			{
				field: "worker",
				title: "${worker}",
				sortable:false,
				filterable: false,
				width: 60,
				attributes: {
				    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
				},headerAttributes:{
					style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
			    },groupHeaderTemplate:'작업자 : #=aggregates.worker.max#'
			},
    		{
                field: "prdNo",
                title: "${prdct_machine_line}",
                width: getElSize(300),
                sortable:false,
                filterable: false,
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },
            {
                field: "oprNo",
                title: "${operation}",
                sortable:false,
                filterable: false,
                width: getElSize(200),
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },
            {
                field: "name",
                title: "${device}",
                width: getElSize(250),
                sortable:false,
                filterable: false,
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },
            /* {
                title: "${uph_analysis}",
                width: 200,
                headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },
                columns: [{
                        title: "Cycle<br>Time",
                        headerAttributes:{
                        	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                        },
                        columns: [ {
                        	title:"${minute}",
                            field: "cylTmMi",
                            width: 40,
                            attributes: {
                                style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            },headerAttributes:{
                            	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            }
                        },{
                        	title:"${second}",
                            field: "cylTmSec",
                            width: 40,
                            attributes: {
                                style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            },headerAttributes:{
                            	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            }
                        }]
                    },{
                    	title:"Ca<br>vi<br>ty",
                    	field:"cvt",
                    	width: 40,
                        attributes: {
                            style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                        },headerAttributes:{
                        	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                        }
                    },{
                    	title:"U<br>P<br>H",
                    	field:"uph",
                    	width: 40,
                        attributes: {
                            style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                        },headerAttributes:{
                        	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                        }
                    }]
            }, */
            /* {
                field: "date",
                title: "${date_}",
                width: 60,
                sortable:false,
                filterable:{
   				 multi: true,
   	             search: true
   				},
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            }, */
            {
                field: "nd",
                title: "  ${division}",
                width: 60,
                sortable:false,
                filterable:{
  					multi: true,
     	            search: true
      			},
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },/* {
            	field:"waitTime",
            	title: "비가동<br>(분)",
            	width: 60,
                sortable:false,
                filterable: false,
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            }, */
            /* {
                
                title: "${performance_analysis}",
                headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                },
                columns: [{
                			field: "worker",
		                	title: "${worker}",
		                	width: 60,
                            attributes: {
                                style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            },headerAttributes:{
                            	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            }
		                },{
		                	 field: "workTm",
		                	 title: "${working_time}",
		                	 width: 60,
	                            attributes: {
	                                style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
	                            },headerAttributes:{
	                            	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
	                            }
		                },{
		                	field:"capa",
		                	title: "${working_capa}",
		                	width: 60,
                            attributes: {
                                style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            },headerAttributes:{
                            	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            }
		                },{
		                	field:"target",
		                	title: "${prd_plan}",
		                	width: 60,
                            attributes: {
                                style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            },headerAttributes:{
                            	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            }
		                },{
                        title: "${prdct_performance}",
                        headerAttributes:{
                        	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                        },
                        columns: [ {
                        	title:"${device}",
                            field: "cnt",
                            width: 80,
                            attributes: {
                                style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            },headerAttributes:{
                            	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            }
                        },{
                        	title:"HMI",
                            field: "partCyl",
                            width: 60,
                            attributes: {
                                style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            },headerAttributes:{
                            	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            }
                        },{
                        	title:"${worker_performance2}",
                        	field: "workerCnt",
                        	width: 60,
                            attributes: {
                                style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            },headerAttributes:{
                            	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            }
                        },{
                        	title:"${diff}",
                        	field:"diff_cnt",
                        	width: 50,
                            attributes: {
                                style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            },headerAttributes:{
                            	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            }
                        }]
                    },{
                    	title:"${faulty}",
                    	field:"faultCnt",
                    	width: 60,
                        attributes: {
                            style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                        },headerAttributes:{
                        	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                        }
                    },{
                    	title:"${ok_ratio}",
                    	field:"okRatio",
                    	width: 40,
                        attributes: {
                            style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                        },headerAttributes:{
                        	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                        }
                    },{
                    	title:"${achievement_ratio}",
                    	field:"goalRatio",
                    	width: 40,
                        attributes: {
                            style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                        },headerAttributes:{
                        	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                        }
                    }]
            }, */
            /* {
		                	 field: "workTm",
		                	 title: "${working_time}",
		                	 width: 60,
	                            attributes: {
	                                style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
	                            },headerAttributes:{
	                            	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
	                            }
		                } ,{
		                	field:"capa",
		                	title: "${working_capa}",
		                	width: 60,
                            attributes: {
                                style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            },headerAttributes:{
                            	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            }
		                },*/
            
             {
                
                title: "작업자 근태",
                headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                },
                columns: [{
		                	 field: "sDate",
		                	 title: "출근시간",
		                     sortable:false,
		                     filterable: false,
		                	 width: 60,
	                            attributes: {
	                                style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
	                            },headerAttributes:{
	                            	style: "text-align: center; font-size:" + getElSize(30) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
	                            }
		                },{
		                	field:"eDate",
		                	title: "퇴근시간",
		                    sortable:false,
		                    filterable: false,
		                	width: 60,
                            attributes: {
                                style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            },headerAttributes:{
                            	style: "text-align: center; font-size:" + getElSize(30) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                            }
		                },{
		                	field:"workTime",
		                	title: "근무시간<br>(시간)",
		                    sortable:false,
		                    filterable: false,
		                	width: 60,
                            attributes: {
                                style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            },headerAttributes:{
                            	style: "text-align: center; font-size:" + getElSize(30) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                            }
		                
		                },{
		                	field:"overTime",
		                	title: "특근시간",
		                    sortable:false,
		                    filterable: false,
		                	width: 60,
		                	template: "#= (overTime == null || overTime == undefined || overTime == '') ? '0' : overTime #",
                            attributes: {
                                style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            },headerAttributes:{
                            	style: "text-align: center; font-size:" + getElSize(30) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                            }
		                
		                },{
		                	field:"attandTime",
		                	title: "근태시간",
		                	width: 60,
		                    sortable:false,
		                    filterable: false,
		                    template: "#= (attandTime == null || attandTime == undefined || attandTime == '') ? '0' : attandTime #",
                            attributes: {
                                style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                            },headerAttributes:{
                            	style: "text-align: center; font-size:" + getElSize(30) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                            }
		                
		                }]
            }, 
            {
                
                title: "작업 실적",
                headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                },
                columns: [
               	{
                	field:"target",
                	title: "목표수량",
                    sortable:false,
                    filterable: false,
                	width: 60,
                       attributes: {
                           style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                       },headerAttributes:{
                       	style: "text-align: center; font-size:" + getElSize(30) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                       }
                },
                {
                	title:"실적 수량",
                    field: "cnt",
                    width: 80,
                    sortable:false,
                    filterable: false,
                    attributes: {
                        style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                    },headerAttributes:{
                    	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                    }
                },{
                	title:"${achievement_ratio}",
                	field:"goalRatio",
                	width: 40,
                	sortable:false,
                	filterable: false,
                	aggregates: ["average"],
                    attributes: {
                        style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                    },headerAttributes:{
                    	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                    }
                },{
                	title:"순위",
                	field: "rank",
                	width: 60,
                	sortable:true,
                	hidden:true,
               	 	filterable:{
      					multi: true,
         	            search: true
          			},
                    attributes: {
                        style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                    },headerAttributes:{
                    	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                    }
                },{
                	title:"${faulty}",
                	field:"faultCnt",
                	width: 60,
                	sortable:false,
                	filterable: false,
                    attributes: {
                        style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                    },headerAttributes:{
                    	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                    }
                },{
                	title:"${ok_ratio}",
                	field:"okRatio",
                	width: 40,
                	sortable:false,
                	filterable: false,
                    attributes: {
                        style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                    },headerAttributes:{
                    	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                    } 
                }]
            }
            ]
    	}).data("kendoGrid");
	    	
	});
	
	</script>
	
	<!-- 생산 차종 Multiple 선택을 위한 부분 -->
	<script type="text/javascript">
	var prdNo_dataSource, multiselect;
	var popup;
	
	function clearMultiSelect() {
	    var multiselect = $("#multiselect").data("kendoMultiSelect");
	    multiselect.value("");
	    multiselect.trigger("change");
	  }
	
	$(function(){
		multiselect = $("#multiselect").kendoMultiSelect({
			clearButton: false,
		    autoClose: true,
		    dataTextField: "prdNo",
		    dataValueField: "prdNo",
		    placeholder: "생산 차종 선택",
		    close: function(e) {
		    },
		    change:function(e){
		    }
		}).data("kendoMultiSelect");
		
		popup = $("#popup").kendoPopup({
			anchor: $("#print"),
			position: "bottom left"
		}).data("kendoPopup");
	})
	
	function pop_print(){
		win = window.open();
        self.focus();
        win.document.open();
        win.document.write('<'+'html'+'><'+'head'+'><'+'style'+'>');
        win.document.write('body, td { font-family: Verdana; font-size: 10pt;}');
        win.document.write('<'+'/'+'style'+'><'+'/'+'head'+'><'+'body'+'>');
        win.document.write(document.getElementById('print_table').innerHTML);
        win.document.write('<'+'/'+'body'+'><'+'/'+'html'+'>');
        win.document.close();
        win.print();
        win.close();
	}
	
	$(function(){
		$("input#sDate").datepicker({
			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
			onSelect:function(){
			}
		})
		$("input#eDate").datepicker({
			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
			onSelect:function(){
			}
		})
		$(".day").css({
			"background" : "green",
			"color" : "white"
		})
	})
	
	
	 var startDate,
     endDate,
     selectCurrentWeek = function () {
         window.setTimeout(function () {
             $('#sDate,#eDate').datepicker('widget').find('.ui-datepicker-current-day a').addClass('ui-state-active')
         }, 1);
     };
	
	function daily(){
		
		$( "input#sDate" ).datepicker( "destroy" );
		$( "input#eDate" ).datepicker( "destroy" );
		
		$('.ui-weekpicker').on('mousemove', 'tr', function () {
	        $(this).find('td a').removeClass('ui-state-hover');
	    });
		
		$("input#sDate").datepicker({
			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
			onSelect:function(){
				//showLoading()
			}
		})
		$("input#eDate").datepicker({
			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
			onSelect:function(){
				//showLoading()
			}
		}) 
		
		$("#dailyRange").css({
			"display" : "inline"
		})
		$("#weeklyRange").css({
			"display" : "none"
		})
		$("#monthlyRange").css({
			"display" : "none"
		})
		
		$(".day").css({
			"background" : "green",
			"color" : "white"
		})
		$(".week").css({
			"background" : "rgb(121, 218, 76)",
			"color" : "black"
		}) 
		$(".month").css({
			"background" : "rgb(121, 218, 76)",
			"color" : "black"
		})
		$("#actionBtn").css({
			"display":"inline"
		})
	}
	
	function weekly(){
		
		$( "input#sDate" ).datepicker( "destroy" );
		$( "input#eDate" ).datepicker( "destroy" );
		
		$('input#sDate').datepicker({
			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
	        "showOtherMonths": false,
	        "selectOtherMonths": false,
	        "onSelect": function (dateText, inst) {
	            var date = $(this).datepicker('getDate'),
	                dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
	            startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
	            endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
	            $('#sDate').val($.datepicker.formatDate(dateFormat, startDate, inst.settings));
	            $('#eDate').val($.datepicker.formatDate(dateFormat, endDate, inst.settings));
	            selectCurrentWeek();
	        },
	        "beforeShow": function () {
	            selectCurrentWeek();
	        },
	        "beforeShowDay": function (date) {
	            var cssClass = '';
	            if (date >= startDate && date <= endDate) {
	                cssClass = 'ui-datepicker-current-day';
	            }
	            return [true, cssClass];
	        },
	        "onChangeMonthYear": function (year, month, inst) {
	            selectCurrentWeek();
	        }
	    }).datepicker('widget').addClass('ui-weekpicker');
	    
	    $('input#eDate').datepicker({
	    	"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
	        "showOtherMonths": false,
	        "selectOtherMonths": false,
	        "onSelect": function (dateText, inst) {
	            var date = $(this).datepicker('getDate'),
	                dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
	            startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
	            endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
	            $('#sDate').val($.datepicker.formatDate(dateFormat, startDate, inst.settings));
	            $('#eDate').val($.datepicker.formatDate(dateFormat, endDate, inst.settings));
	            selectCurrentWeek();
	        },
	        "beforeShow": function () {
	            selectCurrentWeek();
	        },
	        "beforeShowDay": function (date) {
	            var cssClass = '';
	            if (date >= startDate && date <= endDate) {
	                cssClass = 'ui-datepicker-current-day';
	            }
	            return [true, cssClass];
	        },
	        "onChangeMonthYear": function (year, month, inst) {
	            selectCurrentWeek();
	        }
	    }).datepicker('widget').addClass('ui-weekpicker');
	    
	    
	    $('.ui-weekpicker').on('mousemove', 'tr', function () {
	        $(this).find('td a').addClass('ui-state-hover');
	    });
	    $('.ui-weekpicker').on('mouseleave', 'tr', function () {
	    	$(this).find('td a').removeClass('ui-state-hover');
	    });
		
		$("#dailyRange").css({
			"display" : "inline"
		})
		$("#monthlyRange").css({
			"display" : "none"
		})
		$(".day").css({
			"background" : "rgb(121, 218, 76)",
			"color" : "black"
		})
		$(".week").css({
			"background" : "green",
			"color" : "white"
		})
		$(".month").css({
			"background" : "rgb(121, 218, 76)",
			"color" : "black"
		})
		$("#actionBtn").css({
			"display":"inline"
		})
	}
	
	function monthly(){
		$( "input#sDate" ).datepicker( "destroy" );
		$( "input#eDate" ).datepicker( "destroy" );
		
		$('input#sDate').datepicker( {
			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
	        changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'yy mm',
	        onClose: function(dateText, inst) { 
	            var month = Math.abs($("#ui-datepicker-div .ui-datepicker-month :selected").val()) + 1;
	            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	            if(month<10){
	            	month = '0'+month.toString()
	            }
	            
	            $('input#sDate').val(year+'-'+month.toString()+'-'+'01');
	            $('input#eDate').val(year+'-'+month.toString()+'-'+31);
	        }
	    });
		
		$('input#eDate').datepicker( {
			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
	        changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'yy mm',
	        onClose: function(dateText, inst) { 
	            var month = Math.abs($("#ui-datepicker-div .ui-datepicker-month :selected").val()) + 1;
	            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	            if(month<10){
	            	month = '0'+month.toString()
	            }
	            
	            $('input#sDate').val(year+'-'+month.toString()+'-'+'01');
	            $('input#eDate').val(year+'-'+month.toString()+'-'+31);
	        }
	    });
		
		$("#dailyRange").css({
			"display" : "inline"
		})
		
		$(".day").css({
			"background" : "rgb(121, 218, 76)",
			"color" : "black"
		})
		$(".week").css({
			"background" : "rgb(121, 218, 76)",
			"color" : "black"
		})
		$(".month").css({
			"background" : "green",
			"color" : "white"
		})
		$("#actionBtn").css({
			"display":"inline"
		})
	}
	</script>
	<!-- 생산 차종 Multiple 선택을 위한 부분 -->
	
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>
	<div id="popup">등록 예정입니다.</div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/order_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<table id="content_table" style="width: 100%;">
						<tr>
							<Td style="color: white; ">
								<div style="display:inline; float: left;">
									<spring:message code="prdct_machine_line"></spring:message>
									<select id="multiselect" multiple="multiple"></select>
									<button class="reset" style="margin-left: 0px !important" onclick="clearMultiSelect()">초기화</button>
								</div>
								
								<div class="rangeSelector" style="display: -webkit-inline-box;">
									<button id="datePicker" class="day" onclick="daily()">일간</button>					
									<button id="datePicker" class="week" onclick="weekly()">주간</button>					
									<button id="datePicker" class="month" onclick="monthly()">월간</button>
								</div>
								
								<div id="dailyRange" style="display:inline">
									<spring:message code="search_term"></spring:message>
									<input type="text" id="sDate">  ~  <input type="text" id="eDate">
								</div>
								
							  	<div id="actionBtn"style="display: inline; float: right; margin:auto;">
							  		<spring:message code="date_"></spring:message>
								  	<select id="workIdx" onchange="showLoading()" style="vertical-align: middle;">
										<option value="2"><spring:message code="day"></spring:message></option> 
										<option value="1"><spring:message code="night"></spring:message></option> 
										<option value="0">전체</option>
									</select>
									<button id="search" onclick="getPrdData()" style="cursor: pointer;"><i class="fa fa-search" aria-hidden="true"></i></button>
									<button id="print" onclick="pop_print()"><i class="fa fa-print" aria-hidden="true"></i> 프린트</button>
							  	</div>
							</td>
						</Tr>
					</table>
					<div id="grid" style="background : black;"></div>
				</td>
				
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class="nav_span"></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
		</table>
	 </div>
	<div id="print_table" style="display: none;"></div>
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	