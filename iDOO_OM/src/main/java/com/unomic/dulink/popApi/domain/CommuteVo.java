package com.unomic.dulink.popApi.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CommuteVo {
	
	String worker;
	String ty_start;
	String date;
	String start_time;
	String bigo;
	String bigo_cd;
	String pop_id;
	Integer is_outing;
	
}
